class Category {
  final String icon, title;

  Category({required this.icon, required this.title});
}

List<Category> demo_categories = [
  Category(
    icon: "assets/icons/dress.svg",
    title: "Đầm",
  ),
  Category(
    icon: "assets/icons/shirt.svg",
    title: "Áo Thun",
  ),
  Category(
    icon: "assets/icons/pants.svg",
    title: "Quần Dài",
  ),
  Category(
    icon: "assets/icons/Tshirt.svg",
    title: "Áo Sơ Mi",
  ),
];
