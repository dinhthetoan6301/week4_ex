import 'package:flutter/material.dart';

class Product {
  final String image, title;
  final int price;
  final Color bgColor;

  Product({
    required this.image,
    required this.title,
    required this.price,
    this.bgColor = const Color(0xFFEFEFF2),
  });
}

List<Product> demo_product = [
  Product(
    image: "assets/images/product_0.png",
    title: "Ao Thun Dài Trơn",
    price: 165000,
    bgColor: const Color(0xFFFEFBF9),
  ),
  Product(
    image: "assets/images/product_1.png",
    title: "Quần Jogger Đen",
    price: 200000,
  ),
  Product(
    image: "assets/images/product_2.png",
    title: "Quần Jogger Xám",
    price: 180000,
    bgColor: const Color(0xFFF8FEFB),
  ),
  Product(
    image: "assets/images/product_3.png",
    title: "Quần Jogger",
    price: 149000,
    bgColor: const Color(0xFFEEEEED),
  ),
];

List<Product> demo_product2 = [
  Product(
    image: "assets/images/product_5.png",
    title: "Quần Jogger 3",
    price: 450000,
    bgColor: const Color(0xFFFEFBF9),
  ),
  Product(
    image: "assets/images/product_6.png",
    title: "Quần Jogger 2",
    price: 99000,
  ),
  Product(
    image: "assets/images/product_7.png",
    title: "Quần Jogger 1",
    price: 180000,
    bgColor: const Color(0xFFF8FEFB),
  ),
  Product(
    image: "assets/images/product_8.png",
    title: "Quần Jogger 4",
    price: 159000,
    bgColor: const Color(0xFFEEEEED),
  ),
];

List<Product> demo_product3 = [
  Product(
    image: "assets/images/product_0.png",
    title: "Ao Thun Dài Trơn",
    price: 165000,
    bgColor: const Color(0xFFFEFBF9),
  ),
  Product(
    image: "assets/images/product_1.png",
    title: "Quần Jogger Đen",
    price: 200000,
  ),
  Product(
    image: "assets/images/product_5.png",
    title: "Quần Jogger 3",
    price: 450000,
    bgColor: const Color(0xFFF8FEFB),
  ),
  Product(
    image: "assets/images/product_8.png",
    title: "Quần Jogger 4",
    price: 159000,
    bgColor: const Color(0xFFEEEEED),
  ),
];