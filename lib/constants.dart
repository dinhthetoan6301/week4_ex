import 'package:flutter/material.dart';

const Color primaryColor = Color.fromARGB(255, 180, 129, 113);
const Color bgColor = Color.fromARGB(255, 168, 204, 164);

const double defaultPadding = 16.0;
const double defaultBorderRadius = 12.0;
